FROM openjdk:11 as BUILD
COPY root /root
RUN cd root && jar -cvf bodgeit.war .
FROM tomcat:8.0
COPY --from=BUILD root/bodgeit.war /usr/local/tomcat/webapps
